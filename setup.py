import setuptools

with open("README.md", "r") as readme_fh:
    long_description = readme_fh.read()

setuptools.setup(
    name="solvhybrid",
    version="0.0.1",
    author="Carles Martí",
    author_email="carles.marti2@gmail.com",
    description="Program to obtain adsorption free energies of organic "
                "molecules on slabs from Thermodynamic Integrations ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lch_interfaces/solvhybrid",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        # "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
