import logging

from ase import Atoms, Atom

logger = logging.getLogger('SolvHybrid')


def save_amber_parms(pdb_file: str, target_dir: str, write_box=False):
    """Save non-solvated coordinates and parameters for vacuum simulations

    @param pdb_file: The path/name of pdb file of the adsorbate surface.
    @param target_dir: The path/name of pdb file of the pristine surface.
    @param write_box: Whether to write box information or not.
    """
    import os.path
    from subprocess import Popen
    from ase.io.formats import filetype
    from modules.utilities import check_bak

    # File checks
    if not isinstance(pdb_file, str):
        err_msg = f"'{pdb_file}' must be a string containing the file path."
        logger.error(err_msg)
        raise ValueError(err_msg)
    if not os.path.isfile(pdb_file):
        err_msg = f"File '{pdb_file}' not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    if not filetype(pdb_file) == 'proteindatabank':
        err_msg = f"File '{pdb_file}' is not a pdb file."
        logger.error(err_msg)
        raise ValueError(err_msg)
    if not os.path.isdir(target_dir):
        err_msg = f"'{target_dir}' not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)

    # Create tleap input file
    modules_dir = os.path.dirname(__file__)
    tleap_inp = f"loadoff tip3pbox.off\n" \
                f"source leaprc.gaff2\n" \
                f"source {modules_dir}/leaprc.uff\n" \
                f"loadamberparams {modules_dir}/uff.dat\n" \
                f"loadamberparams {modules_dir}/music.dat\n" \
                f"loadoff {modules_dir}/uff.lib\n" \
                f"system = loadpdb {pdb_file}\n" \

    if write_box:
        tleap_inp += "setbox system vdw\n"
    tleap_inp += f"saveamberparm system {target_dir}/uncharged_prmtop " \
                 f"{target_dir}/inpcrd\n" \
                 f"quit\n"
    check_bak("tleap/tleap-parm.inp")
    with open("tleap/tleap-parm.inp", "w") as tleap_fh:
        tleap_fh.write(tleap_inp)

    # Execute tleap
    with open("tleap/tleap-parm.log", "w") as tleap_log:
        Popen("tleap -f tleap/tleap-parm.inp", shell=True, stdout=tleap_log,
              stderr=tleap_log).wait()

    logger.info(f"Saved amber parameters for {pdb_file}.")


def add_special_atoms(symbol_pairs: list):
    """Allows ase to use custom elements with symbols not in the periodic table.

    This function adds new chemical elements to be used by ase. Every new custom
    element must have a traditional partner (present in the periodic table)
    from where all the element properties are going to be taken of.
    Eg. symbol_pairs = [('Fe1', 'Fe'), ('Fe2', 'Fe'), ('O1', 'O')]
    @param symbol_pairs: List of tuples (ie. pairs) of chemical symbols.
        Every tuple must contain two symbols. The first must be the one of the
        new (custom) symbol and the second the traditional one (present on the
        periodic table) where all the properties are going to be used to define
        the first.
    """
    # TODO Add vdw_radii, gsmm and aml (smaller length)
    import numpy as np
    from ase import data
    for pair in symbol_pairs:
        data.chemical_symbols += [pair[0]]
        z_orig = data.atomic_numbers[pair[1]]
        orig_iupac_mass = data.atomic_masses_iupac2016[z_orig]
        orig_com_mass = data.atomic_masses_common[z_orig]
        data.atomic_numbers[pair[0]] = max(data.atomic_numbers.values()) + 1
        data.atomic_names += [pair[0]]
        data.atomic_masses_iupac2016 = np.append(data.atomic_masses_iupac2016,
                                                 orig_iupac_mass)
        data.atomic_masses = data.atomic_masses_iupac2016
        data.atomic_masses_common = np.append(data.atomic_masses_common,
                                              orig_com_mass)
        data.covalent_radii = np.append(data.covalent_radii,
                                        data.covalent_radii[z_orig])
        data.reference_states += [data.reference_states[z_orig]]


def set_proper_masses(atoms: Atoms, symbol_pairs: list):
    """Sets the proper masses of atoms with custom chemical symbols

    @param atoms: the ase.Atoms object for which to fix the masses.
    @param symbol_pairs: The pairs of custom-traditional chemical symbols:
        Custom symbols will take the masses of the traditional ones.
        eg. symbol_pairs = [("H1", "H"), ("H2", "H")]
    """
    from copy import deepcopy
    atoms2 = deepcopy(atoms)
    for pair in symbol_pairs:
        atoms2.set_chemical_symbols([sym.replace(pair[0], pair[1])
                                     for sym in atoms2.get_chemical_symbols()])
    atoms.set_masses(atoms2.get_masses())
    return atoms


def save_pdb(atms: Atoms, pdb_file: str):
    """Write an ase.Atoms object to an amber-compatible PDB file.

    @param atms: ase.Atoms object of the given system
    @param pdb_file: The amber-compatible pdb file.
    """
    from ase.geometry import cell_to_cellpar
    cellpar = cell_to_cellpar(atms.cell)
    with open(pdb_file, "w") as pdb_fh:
        pdb_fh.write('CRYST1%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f P 1\n' %
                     (cellpar[0], cellpar[1], cellpar[2], cellpar[3],
                      cellpar[4], cellpar[5]))
        for atm in atms:
            idx = atm.index + 1
            res_name = atm.symbol
            if atms.info:
                for key in atms.info:
                    if isinstance(atms.info[key], list):
                        if atm.tag in atms.info[key]:
                            res_name = key
            if not atm.tag:
                atm.tag = idx
            pdb_fh.write("ATOM  " + str(
                "{:5d} {:^4s} {:3s}  {:4d}   {:8.3f}{:8.3f}{:8.3f}"
                "".format(idx, atm.symbol, res_name, atm.tag,
                          atm.position[0], atm.position[1], atm.position[2]))
                         + "  0.00  0.00\n")


def read_pdb(pdb_file: str, special_atoms=None):
    """Reads a PDB file and keeps the special atom labels and the residue info.

    @param pdb_file: The pdb file to read.
    @param special_atoms: The pairs of custom-traditional chemical symbols to
        relate the former to the latter.
        eg. special_atoms = [("H1", "H"), ("H2", "H")]
    @return: an ase.Atoms object.
    """
    import numpy as np
    from ase.geometry import cellpar_to_cell
    if special_atoms is not None and not isinstance(special_atoms, list):
        err_msg = "'special_atoms' must be a list of pairs (tuples) of " \
                  "chemical symbols."
        logger.error(err_msg)
        raise ValueError(err_msg)
    if special_atoms:
        add_special_atoms(special_atoms)
    pdb_fh = open(pdb_file)
    atms = Atoms()
    for line in pdb_fh:
        if line.startswith("CRYST1"):
            cellpar = [float(line[6:15]),   # a
                       float(line[15:24]),  # b
                       float(line[24:33]),  # c
                       float(line[33:40]),  # alpha
                       float(line[40:47]),  # beta
                       float(line[47:54])]  # gamma
            atms.cell = cellpar_to_cell(cellpar)
            atms.pbc = True
        elif line.startswith("ATOM"):
            line_list = line.split()
            symbol = line_list[2]
            res_name = line_list[3]
            res_num = int(line_list[4])
            position = np.array(line_list[5:8])
            atm = Atom(symbol, position, res_num)
            atms.append(atm)
            # Associate residue numbers to residue names
            if res_name != symbol:
                if res_name not in atms.info:
                    atms.info[res_name] = []
                if res_num not in atms.info[res_name]:
                    atms.info[res_name].append(res_num)
    pdb_fh.close()
    return atms
